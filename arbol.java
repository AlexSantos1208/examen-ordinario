/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

/**
 *
 * @author Alexander De La Cruz Santos
 */
public class Arbol {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int dato;
        String nombre;
        ArbolBinario miArbol = new ArbolBinario();
        miArbol.agregarNodo(79, "A");
        miArbol.agregarNodo(82, "L");
        miArbol.agregarNodo(76, "E");
        miArbol.agregarNodo(65, "E");
        miArbol.agregarNodo(78, "R");
        miArbol.agregarNodo(68, "D");
        miArbol.agregarNodo(73, "C");
        miArbol.agregarNodo(69, "S");
        miArbol.agregarNodo(67, "A");
        miArbol.agregarNodo(72, "O");
        miArbol.agregarNodo(83, "S");
        
        
        System.out.println("PreOrden");
        if (!miArbol.estaVacio()){
            miArbol.preOrden(miArbol.raiz);
        }
        
        
    }
    
}
